/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 12:46:44 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/18 10:20:55 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

static int	round_sqrt(int nr)
{
	int		r;

	r = 1;
	while (r * r < nr)
		r++;
	return (r);
}

static void	ft_putstr(char *str)
{
	while (*str != '\0')
	{
		write(1, str, 1);
		str++;
	}
}

static void	write_zero(t_grid *grid)
{
	int i;

	i = -1;
	while (++i < 16)
		(*grid).grid_arr[i] = 0;
}

static void	stupid_split_t(t_tetro **tetro_p, int tetro_nr)
{
	normalize_input(tetro_p, tetro_nr);
	get_tetro_dim(tetro_p, tetro_nr);
}

int			main(int argc, char **argv)
{
	t_tetro			*tetro_p;
	int				tetro_nr;
	t_grid			grid;

	if (argc != 2)
	{
		ft_putstr("usage: ./fillit [input_file]\n");
		return (0);
	}
	if (get_input(argv[1], &tetro_p, &tetro_nr) < 0 ||
		!(grid.grid_arr = (unsigned short*)malloc(sizeof(unsigned short) * 16)))
	{
		ft_putstr("error\n");
		return (0);
	}
	stupid_split_t(&tetro_p, tetro_nr);
	grid.grid_size = round_sqrt(tetro_nr * 4);
	write_zero(&grid);
	while (!solve_grid(&grid, tetro_p, 0, tetro_nr))
	{
		write_zero(&grid);
		grid.grid_size++;
	}
	print_grid(grid.grid_size, tetro_p, tetro_nr);
	return (0);
}
