/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_tetro.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:49:42 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 14:50:52 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

int		place_tetro(unsigned short **grid_arr, t_tetro *tetro, int x, int y)
{
	if (!(*(unsigned long*)(*tetro).row >> x &
				*(unsigned long*)(*grid_arr + y)))
	{
		(*tetro).start_x = x;
		(*tetro).start_y = y;
		*(unsigned long*)(*grid_arr + y) ^= *(unsigned long*)(*tetro).row >> x;
		return (1);
	}
	return (0);
}
