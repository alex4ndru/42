/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normalize_input.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 14:18:18 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/12 16:51:19 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"
#include <fcntl.h>

static void	get_ttr_limits(t_tetro **tetro_p, int *min_x, int *min_y, int i)
{
	int x;
	int y;

	*min_x = 3;
	*min_y = 3;
	y = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			if ((*tetro_p)[i].map[y][x] != '.')
			{
				if (y < *min_y)
					*min_y = y;
				if (x < *min_x)
					*min_x = x;
			}
			x++;
		}
		y++;
	}
}

static void	normalize_position(t_tetro **tetro_p, int min_x, int min_y, int i)
{
	int y;
	int x;

	y = min_y;
	while (y < 4)
	{
		x = min_x;
		while (x < 4)
		{
			(*tetro_p)[i].map[y - min_y][x - min_x] = (*tetro_p)[i].map[y][x];
			if ((*tetro_p)[i].map[y - min_y][x - min_x] == '#')
			{
				(*tetro_p)[i].map[y - min_y][x - min_x] = (*tetro_p)[i].symbol;
			}
			if (y - min_y != y || x - min_x != x)
				(*tetro_p)[i].map[y][x] = '.';
			x++;
		}
		y++;
	}
}

static void	b_b_mask(t_tetro **tetro_p, int i)
{
	int	x;
	int	y;

	y = 0;
	while (y < 4)
	{
		x = 0;
		while (x < 4)
		{
			if ((*tetro_p)[i].map[y][x] != '.')
				(*tetro_p)[i].row[y] |= ((unsigned short)1 << (16 - 1 - x));
			x++;
		}
		y++;
	}
}

void		normalize_input(t_tetro **tetro_p, int tetro_nr)
{
	int		min_x;
	int		min_y;
	int		i;

	i = 0;
	while (i < tetro_nr)
	{
		get_ttr_limits(tetro_p, &min_x, &min_y, i);
		normalize_position(tetro_p, min_x, min_y, i);
		b_b_mask(tetro_p, i);
		i++;
	}
}
