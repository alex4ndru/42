/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove_tetro.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 14:38:01 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/09 14:40:49 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "fillit.h"

void	remove_tetro(unsigned short **grid_arr, t_tetro *tetro, int x, int y)
{
	*(unsigned long*)(*grid_arr + y) ^= (*(unsigned long*)(*tetro).row >> x);
}
