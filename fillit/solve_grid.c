/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 13:13:24 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 14:18:19 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

#include <stdio.h>

static void	check_same(int current, t_tetro *tetro_arr, int *y, int *same)
{
	if (current > 0 && (*(size_t*)(tetro_arr[current]).row ^
				*(size_t*)(tetro_arr[current - 1]).row) == 0)
	{
		*y = tetro_arr[current - 1].start_y - 1;
		*same = 1;
	}
}

int			solve_grid(t_grid *grid, t_tetro *tetro_arr, int current, int max)
{
	int		x;
	int		y;
	t_tetro	*tetro;
	int		same;

	same = 0;
	y = -1;
	check_same(current, tetro_arr, &y, &same);
	if (current == max)
		return (1);
	tetro = &(tetro_arr[current]);
	while (++y < grid->grid_size - tetro->height + 1)
	{
		x = -1;
		if (same == 1 && y == (tetro_arr[current - 1].start_y - 1))
			x = tetro_arr[current - 1].start_x - 1;
		while (++x < grid->grid_size - tetro->width + 1)
			if (place_tetro(&(grid->grid_arr), tetro, x, y) == 1)
			{
				if (solve_grid(grid, tetro_arr, current + 1, max))
					return (1);
				remove_tetro(&(grid->grid_arr), tetro, x, y);
			}
	}
	return (0);
}
