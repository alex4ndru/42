/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 11:45:57 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/12 11:58:51 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

void	print_grid(int grid_size, t_tetro *ttr, int tetro_nr)
{
	int		i;
	int		j;
	int		k;
	char	c;

	i = 0;
	while (i < grid_size)
	{
		j = -1;
		while (++j < grid_size)
		{
			c = '.';
			k = -1;
			while (c == '.' && ++k < tetro_nr)
				if (i - ttr[k].start_y >= 0 &&
					j - ttr[k].start_x >= 0 && i - ttr[k].start_y < 4 &&
					j - ttr[k].start_x < 4)
					if (ttr[k].map[i - ttr[k].start_y][j - ttr[k].start_x] !=
							'.')
						c = ttr[k].symbol;
			write(1, &c, 1);
		}
		write(1, "\n", 1);
		i++;
	}
}
