/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tetro_dim.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:23:37 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 14:38:29 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

void	get_tetro_dim(t_tetro **tetro_p, int tetro_nr)
{
	size_t	max_y;
	size_t	max_x;
	int		i;
	size_t	j;

	i = -1;
	while (++i < tetro_nr)
	{
		max_y = 0;
		max_x = 0;
		j = -1;
		while (++j < 16)
			if ((*tetro_p)[i].map[j / 4][j % 4] != '.')
			{
				if (max_y < j / 4)
					max_y = j / 4;
				if (max_x < j % 4)
					max_x = j % 4;
			}
		(*tetro_p)[i].height = max_y + 1;
		(*tetro_p)[i].width = max_x + 1;
	}
}
