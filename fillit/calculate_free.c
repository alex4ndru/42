/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:40:40 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 14:49:06 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

static int		rec_free(unsigned short *grid, int grid_size, int y, int x)
{
	int				nr;
	unsigned short	row;

	nr = 0;
	row = (unsigned short)1 << (16 - 1 - x);
	if (!(grid[y] & row))
	{
		grid[y] |= row;
		nr = 1;
		if (x > 0)
			nr += rec_free(grid, grid_size, y, x - 1);
		if (x < grid_size - 1)
			nr += rec_free(grid, grid_size, y, x + 1);
		if (y > 0)
			nr += rec_free(grid, grid_size, y - 1, x);
		if (y < grid_size - 1)
			nr += rec_free(grid, grid_size, y + 1, x);
	}
	return (nr);
}

int				calculate_free(unsigned short *grid,
		int grid_size, int max_free)
{
	int				i;
	int				j;
	int				k;
	int				total_waste;
	unsigned short	*grid2;

	grid2 = (unsigned short*)malloc(sizeof(unsigned short) * 16);
	k = 0;
	while (k < grid_size)
	{
		grid2[k] = 0;
		grid2[k] |= grid[k];
		k++;
	}
	total_waste = 0;
	i = -1;
	while (total_waste <= max_free && ++i < grid_size)
	{
		j = -1;
		while (total_waste <= max_free && ++j < grid_size)
			total_waste += rec_free(grid2, grid_size, i, j) % 4;
	}
	return (total_waste);
}
