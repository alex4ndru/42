/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 12:48:04 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 18:57:06 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# define MAX_SIZE 20

typedef struct		s_tetro
{
	char			map[4][4];
	char			symbol;
	int				height;
	int				width;
	int				start_x;
	int				start_y;
	unsigned short	row[4];
}					t_tetro;

typedef struct		s_grid
{
	unsigned short	*grid_arr;
	int				grid_size;
}					t_grid;

int					get_input(char *file, t_tetro **tetro_p, int *tetro_nr);
void				normalize_input(t_tetro **tetro_p, int tetro_nr);
void				print_grid(int grid_size, t_tetro *tetro, int tetro_nr);
void				get_tetro_dim(t_tetro **tetro_p, int tetro_nr);
int					place_tetro(unsigned short **ga, t_tetro *tr, int x, int y);
void				remove_tetro(unsigned short **ga, t_tetro *tr,
		int x, int y);
int					solve_grid(t_grid *grid, t_tetro *tra, int cur, int max);
int					calculate_free(unsigned short *grid, int grs, int max_free);
unsigned long		build_bit_mask(char bf[4][4]);
void				print_bits_any(size_t size, void *ptr);
void				print_bits_map(size_t size, unsigned char *b);
void				print_n_bits_any(size_t size, void *ptr, int n);
void				print_bits_any_c(size_t size, void *ptr, int color);
void				put_str_c(char *str, int color);
void				put_nbr_c(int nbr, int color);
#endif
