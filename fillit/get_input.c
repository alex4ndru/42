/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/07 16:11:18 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/09 13:01:57 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"
#include <fcntl.h>

#include <stdio.h>

static int	count_hash_nbrs(char *bf, int i)
{
	int count;

	count = 0;
	if (i - 5 >= 0 && bf[i - 5] == '#')
		count++;
	if (i + 5 <= 20 && bf[i + 5] == '#')
		count++;
	if (i - 1 >= 0 && bf[i - 1] == '#')
		count++;
	if (i + 1 <= 20 && bf[i + 1] == '#')
		count++;
	return (count);
}

static int	is_valid_input(char *bf, size_t bfl)
{
	size_t	i;
	int		hcnt;
	int		hnbrs;

	i = -1;
	hcnt = 0;
	hnbrs = 0;
	if (bfl < 20)
		return (-1);
	if (bf[bfl - 1] == '\n')
		bfl--;
	while (++i < bfl)
	{
		if (((i + 1) % 5 != 0 && bf[i] != '#' && bf[i] != '.') ||
				(((i + 1) % 5 == 0) && (bf[i] != '\n')))
			return (-1);
		if (bf[i] == '#')
		{
			hcnt++;
			hnbrs += count_hash_nbrs(bf, i);
		}
	}
	if ((hnbrs != 6 && hnbrs != 8) || hcnt != 4)
		return (-1);
	return (0);
}

int			get_input(char *file, t_tetro **tetro_p, int *tetro_nr)
{
	int		fd;
	char	bf[21];
	size_t	bf_i;
	ssize_t	bfl;

	fd = open(file, O_RDONLY);
	if (fd < 0 || !(*tetro_p = (t_tetro*)malloc(sizeof(t_tetro) * 26)))
		return (-1);
	*tetro_nr = 0;
	while ((bfl = read(fd, bf, 21)) > 0)
	{
		if (is_valid_input(bf, bfl) < 0)
			return (-1);
		bf_i = -1;
		while (++bf_i < 21)
			(*tetro_p)[*tetro_nr].map[bf_i / 5][bf_i % 5] = bf[bf_i];
		(*tetro_p)[*tetro_nr].symbol = *tetro_nr + 'A';
		(*tetro_nr)++;
		bf_i = bfl;
	}
	close(fd);
	if ((bf_i != 20) || *tetro_nr == 0 || *tetro_nr > 26)
		return (-1);
	return (0);
}
