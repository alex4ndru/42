/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:22:06 by czaharia          #+#    #+#             */
/*   Updated: 2018/01/10 14:22:46 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "fillit.h"

t_grid	*build_grid(int size)
{
	t_grid	*grid;
	int		i;
	int		j;

	grid = (t_grid*)malloc(sizeof(*grid));
	grid->size = size;
	grid->map = (char**)malloc(sizeof(char*) * size);
	i = 0;
	while (i < size)
	{
		grid->map[i] = (char*)malloc(sizeof(char) * size);
		j = 0;
		while (j < size)
		{
			grid->map[i][j] = '.';
			j++;
		}
		i++;
	}
	return (grid);
}
