/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/20 15:17:34 by ialexand          #+#    #+#             */
/*   Updated: 2018/01/20 15:19:57 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "get_next_line.h"

int		main()
{
	char	*line;
	int		fd1;
	int		fd2;
	int		fd3;

	fd1 = open("test1", O_RDONLY);
	fd2 = open("test2", O_RDONLY);
	fd3 = open("test3", O_RDONLY);


	get_next_line(fd1, &line);
	printf("test1, line 1: %s\n", line);

	get_next_line(fd2, &line);
	printf("test2, line 1: %s\n", line);

	get_next_line(fd1, &line);
	printf("test1, line 2: %s\n", line);

	get_next_line(fd3, &line);
	printf("test3, line 1: %s\n", line);

	get_next_line(fd2, &line);
	printf("test2, line 2: %s\n", line);

	return (0);
}
