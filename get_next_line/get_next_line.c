/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 11:23:16 by ialexand          #+#    #+#             */
/*   Updated: 2018/01/18 10:52:30 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int			read_rec(t_list *current)
{
	char			bf[BUFF_SIZE + 1];
	int				nr;
	int				done;
	int				i;

	if ((nr = read((int)current->content_size, bf, BUFF_SIZE)) > 0)
	{
		bf[nr] = '\0';
		i = 0;
		done = 0;
		while (done == 0 && i < nr)
		{
			if (bf[i] == '\n')
				done = 1;
			i++;
		}
		current->content = ft_strjoin(current->content, bf);
		if (done == 0)
			read_rec(current);
	}
	if (nr > 1)
		nr = 1;
	return (nr);
}

static int			get_line(t_list *current, char **line)
{
	int				len;
	int				offset;
	int				r;

	r = read_rec(current);
	if (r == -1)
		return (-1);
	len = 0;
	while (((char*)(current->content))[len] != '\n' &&
			((char*)(current->content))[len] != '\0')
		len++;
	if (((char*)(current->content))[len] == '\0' && r == 0)
		return (0);
	offset = len;
	*line = (char*)malloc(sizeof(char) * (len + 1));
	(*line)[len] = '\0';
	len--;
	while (len >= 0)
	{
		(*line)[len] = ((char*)(current->content))[len];
		len--;
	}
	current->content += offset + 1;
	return (1);
}

void				fx(t_list **c, t_list **d, int *f, int fd)
{
	*f = 0;
	if ((*c)->content_size == (size_t)fd)
		*f = 1;
	while (*f == 0 && (*c)->next != NULL)
	{
		(*c) = (*c)->next;
		if ((*c)->content_size == (size_t)fd)
			*f = 1;
	}
	if (*f == 0)
	{
		(*c) = ft_lstnew("", (size_t)fd);
		ft_lstadd(d, *c);
	}
}

int					get_next_line(const int fd, char **line)
{
	static t_list	*data;
	t_list			*current;
	int				found;
	int				r;

	if (fd < 0 || fd == 1 || fd == 2 || !line)
		return (-1);
	*line = 0;
	if (data == NULL)
	{
		data = ft_lstnew("", (size_t)fd);
		r = get_line(data, line);
	}
	else
	{
		current = data;
		fx(&current, &data, &found, fd);
		r = get_line(current, line);
	}
	return (r);
}
