SASTANTUA.
	A team project. Create a program that draws a pyramid of a given size

LIBFT.
	An individual project at 42 that requires to re-create some standard C library functions

Fillit.
	A team project. The goal is to fit some Tetriminos together, into the smallest possible square

BSQ.
	A team project. Given a map and its legend (in the first line of the map file), you have to find the biggest square that you can make without encountering any obstacle.
	In case of multiple squares of the same size it choses the most upper one and then the one which is most left.

GET_NEXT_LINE.
	Objective: Write a function that returns a line read from a file descriptor. The function must be able to manage multiple file descriptors. Lseek function is not allowed.
	Calling the your function in a loop will read the text available on a file descriptor (or more) one line at a time until the end of the text, no matter the size of either the text or one of its lines.

FDF (fil de fer / wireframe).
	Objective: Build a program that will display in 3D the wireframe of a simple mesh
