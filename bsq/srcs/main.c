/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 14:51:59 by ialexand          #+#    #+#             */
/*   Updated: 2017/10/31 12:59:01 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>

int		main(int ac, char **av)
{
	int		i;

	if (ac > 1)
	{
		i = 1;
		while (i < ac)
		{
			if (i > 1 && i < ac)
				ft_putstr("\n");
			if (solve_matrix_file(av[i]) == -1)
			{
				write(2, "map error\n", 10);
				i++;
				continue;
			}
			i++;
		}
	}
	else
	{
		if (solve_matrix_input() == -1)
			write(2, "map error\n", 10);
	}
	return (0);
}
