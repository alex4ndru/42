/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nr_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 12:34:16 by ialexand          #+#    #+#             */
/*   Updated: 2017/10/30 12:37:06 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_nr_size(int nr)
{
	int		divide;
	int		result;

	result = 1;
	divide = 10;
	while (nr / divide != 0)
	{
		divide = divide * 10;
		result++;
	}
	return (result);
}
