/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_first_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:15:18 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 14:18:12 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		check_first_line(char *file_path, t_matrix **m)
{
	t_file	f;

	f.fd = open(file_path, O_RDONLY);
	if (f.fd > 0)
	{
		(*m)->row_nr = 0;
		f.bf[0] = 'x';
		f.col = 0;
		while (f.bf[0] != '\n' && (f.rd = read(f.fd, f.bf, 1)))
		{
			f.nr_str[f.col] = f.bf[0];
			f.col = f.col + 1;
		}
		f.initial_size = f.col;
		f.nr_str[(f.col) - 4] = '\0';
		(*m)->row_nr = ft_atoi(f.nr_str);
		if ((*m)->row_nr == 0 ||
				(f.initial_size - ft_nr_size((*m)->row_nr)) != 4)
			return (-1);
		return (0);
	}
	else
		return (-1);
}
