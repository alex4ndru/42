/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:13:35 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 14:14:20 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		check_file(char *file_path, t_matrix **m)
{
	int		fd;
	int		rd;
	char	bf[BUFF_SIZE + 1];

	if (check_first_line(file_path, m) == -1)
		return (-1);
	get_symbols(file_path, m);
	fd = open(file_path, O_RDONLY);
	if (fd > 0)
	{
		(*m)->col_nr = 0;
		while ((rd = read(fd, bf, BUFF_SIZE)))
			(*m)->col_nr += rd;
		(*m)->col_nr = ((*m)->col_nr - 4 - ft_nr_size((*m)->row_nr) -
				(*m)->row_nr) / (*m)->row_nr;
		return (0);
	}
	else
		return (-1);
}
