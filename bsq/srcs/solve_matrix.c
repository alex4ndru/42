/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_matrix.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:47:19 by czaharia          #+#    #+#             */
/*   Updated: 2017/11/01 12:16:38 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	set_sol(t_sol **sol, int size, int end_y, int end_x)
{
	if (!*sol)
		*sol = malloc(sizeof(*sol));
	(*sol)->size = size;
	(*sol)->end_y = end_y;
	(*sol)->end_x = end_x;
}

void	silly_split_condition(t_matrix **m, int x, int y, t_sol **sol)
{
	int min;

	if ((*m)->matrix[y][x] != 0)
	{
		if (y == 0 && x == 0)
			min = 0;
		else if (y == 0 && x > 0)
			min = get_min(0, 0, (*m)->matrix[y][x - 1]);
		else if (y > 0 && x == 0)
			min = get_min(0, (*m)->matrix[y - 1][x], 0);
		else
			min = get_min((*m)->matrix[y - 1][x - 1],
					(*m)->matrix[y - 1][x], (*m)->matrix[y][x - 1]);
		(*m)->matrix[y][x] = min + 1;
		if (min + 1 > (*sol)->size)
			set_sol(sol, min + 1, y, x);
	}
}

t_sol	*solve_matrix(t_matrix **m)
{
	int		x;
	int		y;
	t_sol	*sol;

	sol = 0;
	set_sol(&sol, 0, 0, 0);
	y = -1;
	while (++y < (*m)->row_nr)
	{
		x = -1;
		while (++x < (*m)->col_nr)
		{
			silly_split_condition(m, x, y, &sol);
		}
	}
	return (sol);
}
