/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_matrix_file.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 13:01:14 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 14:12:02 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		token_tester(t_matrix *m, int i, int j)
{
	return ((i > m->sol->end_y - m->sol->size) &&
		(i <= m->sol->end_y) &&
		(j > m->sol->end_x - m->sol->size) &&
		(j <= m->sol->end_x));
}

void	print_matrix(t_matrix *m)
{
	int i;
	int j;

	i = 0;
	while (i < m->row_nr)
	{
		j = 0;
		while (j < m->col_nr)
		{
			if (m->matrix[i][j] == 0)
				ft_putchar(m->obs);
			else
			{
				if (token_tester(m, i, j))
					ft_putchar(m->mark);
				else
					ft_putchar(m->free);
			}
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

int		solve_matrix_file(char *file_path)
{
	t_matrix	*m;

	m = malloc(sizeof(*m));
	if (check_file(file_path, &m) == -1)
		return (-1);
	if (build_matrix(file_path, &m) == -1)
		return (-1);
	m->sol = solve_matrix(&m);
	print_matrix(m);
	free(m->sol);
	free(m);
	return (0);
}
