/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ialexand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 22:32:49 by ialexand          #+#    #+#             */
/*   Updated: 2017/10/14 00:10:43 by ialexand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_atoi(char *str)
{
	int			semn;
	long long	result;

	semn = 1;
	while (*str == ' ' || (*str >= 9 && *str <= 13))
	{
		str++;
	}
	result = 0;
	while (*str >= '0' && *str <= '9' && *str != '\0')
	{
		result = result * 10 + *str - 48;
		str++;
	}
	return ((int)(result * semn));
}
