/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_matrix.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:18:39 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 14:44:51 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		matrix_build_row(t_matrix **m, char *bf, int row)
{
	int col;

	col = 0;
	while (col < (*m)->col_nr)
	{
		if (bf[col] == (*m)->free)
			(*m)->matrix[row][col] = 1;
		else if (bf[col] == (*m)->obs)
			(*m)->matrix[row][col] = 0;
		else
			return (-1);
		col++;
	}
	return (0);
}

int		build_matrix(char *file_path, t_matrix **m)
{
	int		row;
	int		fd;
	int		rd;
	char	bf[BUFF_SIZE + 1];

	fd = open(file_path, O_RDONLY);
	if (fd > 0)
	{
		(*m)->matrix = (int**)malloc(sizeof(int*) * ((*m)->row_nr));
		rd = read(fd, bf, ft_nr_size((*m)->row_nr) + 4);
		bf[rd] = '\0';
		row = 0;
		while ((rd = read(fd, bf, (*m)->col_nr + 1)))
		{
			bf[rd] = '\0';
			(*m)->matrix[row] = (int*)malloc(sizeof(int) * ((*m)->col_nr));
			if (((*m)->col_nr + 1 - ft_strlen(bf)) != 0)
				return (-1);
			if (matrix_build_row(m, bf, row) == -1)
				return (-1);
			row++;
		}
		return (0);
	}
	return (-1);
}
