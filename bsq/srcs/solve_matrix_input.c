/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_matrix_input.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:46:48 by czaharia          #+#    #+#             */
/*   Updated: 2017/11/01 11:58:53 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		check_input_symbols(t_matrix *m)
{
	if (m->free != m->obs && m->free != m->mark && m->obs != m->mark)
		return (0);
	else
		return (-1);
}

int		check_input_first_line(t_matrix **m)
{
	int		rd;
	char	bf[BUFF_SIZE + 1];
	int		i;

	rd = read(0, bf, BUFF_SIZE);
	bf[rd] = '\0';
	i = 0;
	while (bf[i] != '\n')
		i++;
	(*m)->free = bf[i - 3];
	(*m)->obs = bf[i - 2];
	(*m)->mark = bf[i - 1];
	if (check_input_symbols(*m) == -1)
		return (-1);
	(*m)->row_nr = ft_atoi(bf);
	if ((ft_nr_size((*m)->row_nr) + 3) != i)
		return (-1);
	return (0);
}

void	silly_split(t_matrix **m)
{
	(*m)->sol = solve_matrix(m);
	print_matrix(*m);
	free((*m)->sol);
	free(*m);
}

int		silly_split_test(int current_row, t_matrix **m, int rd)
{
	if (current_row == 0)
		(*m)->col_nr = rd - 1;
	else
	{
		if ((*m)->col_nr != rd - 1)
			return (-1);
	}
	return (0);
}

int		solve_matrix_input(void)
{
	t_matrix	*m;
	int			current_row;
	int			rd;
	char		bf[BUFF_SIZE + 1];

	m = malloc(sizeof(*m));
	if (check_input_first_line(&m) == -1)
		return (-1);
	current_row = 0;
	m->matrix = (int**)malloc(sizeof(int*) * (m->row_nr));
	while (current_row < m->row_nr)
	{
		rd = read(0, bf, BUFF_SIZE);
		bf[rd] = '\0';
		if (silly_split_test(current_row, &m, rd) == -1)
			return (-1);
		m->matrix[current_row] = (int*)malloc(sizeof(int) * (m->col_nr));
		if (matrix_build_row(&m, bf, current_row) == -1)
			return (-1);
		current_row++;
	}
	silly_split(&m);
	return (0);
}
