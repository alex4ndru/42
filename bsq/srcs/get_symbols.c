/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_symbols.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 14:16:34 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 14:16:46 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	get_symbols(char *file_path, t_matrix **m)
{
	t_file	f;
	char	*bf;
	int		size;

	size = ft_nr_size((*m)->row_nr) + 4;
	bf = (char*)malloc(sizeof(char) * size);
	f.fd = open(file_path, O_RDONLY);
	f.rd = read(f.fd, bf, size - 1);
	(*m)->free = bf[size - 4];
	(*m)->obs = bf[size - 3];
	(*m)->mark = bf[size - 2];
}
