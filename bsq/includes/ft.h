/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: czaharia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 15:29:52 by czaharia          #+#    #+#             */
/*   Updated: 2017/10/31 15:30:53 by czaharia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H
# define BUFF_SIZE 4900
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>

typedef struct	s_file
{
	int		fd;
	int		rd;
	char	bf[2];
	int		col;
	char	nr_str[100];
	int		initial_size;
}				t_file;

typedef struct	s_sol
{
	int			size;
	int			end_y;
	int			end_x;
}				t_sol;
typedef struct	s_matrix
{
	int			**matrix;
	int			row_nr;
	int			col_nr;
	t_sol		*sol;
	char		free;
	char		obs;
	char		mark;

}				t_matrix;

void			ft_putchar(char c);
void			ft_putstr(char *str);
void			ft_putnbr(int nb);
int				ft_atoi(char *str);
int				ft_nr_size(int	nr);
int				ft_strlen(char *str);
int				solve_matrix_file(char *file_path);
int				check_file(char *file_path, t_matrix **m);
int				check_first_line(char *file_path, t_matrix **m);
int				solve_matrix_input(void);
int				build_matrix(char *file_path, t_matrix **m);
t_sol			*solve_matrix(t_matrix **m);
int				get_min(int a, int b, int c);
void			get_symbols(char *file_path, t_matrix **m);
int				matrix_build_row(t_matrix **m, char *bf, int row);
void			print_matrix(t_matrix *m);
void			set_sol(t_sol **sol, int size, int end_y, int end_x);
#endif
